import { library } from '@fortawesome/fontawesome-svg-core';
import {faUser,faBriefcase,faMapSigns,faAddressCard,faGraduationCap,faEllipsisV,faGripHorizontal,faMobile,faPen,faCamera,faIdCard,faCheckDouble,faHome,faClipboardList,faCog,faSuitcase,faFileAlt,faPhoneAlt,faPaste} from '@fortawesome/free-solid-svg-icons';


library.add(faUser,faBriefcase,faMapSigns,faAddressCard,faGraduationCap,faEllipsisV,faGripHorizontal,faMobile,faPen,faCamera,faIdCard,faCheckDouble,faHome,faClipboardList,faCog,faSuitcase,faFileAlt,faPhoneAlt,faPaste);