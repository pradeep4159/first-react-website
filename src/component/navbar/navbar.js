import React, { Component } from "react";
import "./navbar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class navbar extends Component {
 
  render() {
    return (
      <div className=" nav-menu">
        <div className="nav-container" >
           <div className="menu-icon-subcontainer">
            <div className="menu"></div>
            <div className="menu"></div>
            <div className="menu"></div>
          </div> 
        </div>

        <div id="menu-items-container" >
          <div class="profile-image-section">
            <div id="profile-image-container">
              <img
                id="profile-image"
                src="../assert/image/avatar.jpg"
                alt=""
              />
            </div>

            <div className="navbar">
              <div>Sean stevenson</div>
              <div>sean stevenson@gmail.com</div>
           
            </div>
          </div>

          <nav>
            <ul className="sidenav-list">
              <li className="sidenav-list-item">
                <a href="#home-container">
                  <FontAwesomeIcon icon="home" className="icon" />
                  Home
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#about-cont">
                  <FontAwesomeIcon icon="user" className="icon" />
                  About
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#exp-cont">
                  <FontAwesomeIcon icon="clipboard-list" className="icon" />
                  Resume
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#service-cont">
                  <FontAwesomeIcon icon="cog" className="icon" />
                  Services
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#portfolio-cont">
                  <FontAwesomeIcon icon="briefcase" className="icon" />
                  Portfolio
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#price-cont">
                  <FontAwesomeIcon icon="paste" className="icon" />
                  Pricing
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#blog-cont">
                  <FontAwesomeIcon icon="file-alt" className="icon" />
                  Blog
                </a>
              </li>
              <li className="sidenav-list-item">
                <a href="#contact-cont">
                  <FontAwesomeIcon icon="phone-alt" className="icon" />
                  Contact
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}

export default navbar;
