import React, { Component } from "react";
import "./experience.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class experience extends Component {
  render() {
    return (
      <div className="main-div" id="exp-cont">
        <div className="heading">
          <p>Experience & Education</p>
        </div>
        <div className="ex-contain">
          <div className="section">
            <div className="sub-section">
              <div className="style">
                <span className="head">Facebook inc.</span>
                <span className="b-text">PRESENT</span>
                <br />
                <span className="color-text">Art Director</span>
                <hr></hr>
                <p>
                  Lorem ipsum dolor sit amet, in quodsi vulputate pro. Ius illum
                  vocent mediocritatem an, cule dicta iriure at. Ubique
                  maluisset vel te, his dico vituperata ut. Pro ei phaedrum
                  maluisset. Ex audire suavitate has, ei quodsi tacimates
                  sapientem sed, pri zril ubique ut. Lorem ipsum dolor sit amet,
                  in quodsi.
                </p>
              </div>
            </div>
          </div>
          <div className="section">
            <div className=" sub-section">
              <div className="style">
                <span className="head">Google inc.</span>
                <span className="b-text">2014-2016</span>
                <br />
                <span className="color-text">Front-end Developer</span>
                <hr></hr>

                <p>
                  Lorem ipsum dolor sit amet, in quodsi vulputate pro. Ius illum
                  vocent mediocritatem an, cule dicta iriure at. Ubique
                  maluisset vel te, his dico vituperata ut. Pro ei phaedrum
                  maluisset. Ex audire suavitate has, ei quodsi tacimates
                  sapientem sed, pri zril ubique ut. Lorem ipsum dolor sit amet,
                  in quodsi.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="ex-contain">
          <div className="section">
            <div className="sub-section">
              <div className="style">
                <span className="head">Abc Inc.</span>
                <span className="b-text">2012-2014</span>
                <br />
                <span className="color-text">Senior Developer</span>
                <hr></hr>
                <p>
                  Lorem ipsum dolor sit amet, in quodsi vulputate pro. Ius illum
                  vocent mediocritatem an, cule dicta iriure at. Ubique
                  maluisset vel te, his dico vituperata ut. Pro ei phaedrum
                  maluisset. Ex audire suavitate has, ei quodsi tacimates
                  sapientem sed, pri zril ubique ut. Lorem ipsum dolor sit amet,
                  in quodsi.
                </p>
              </div>
            </div>
          </div>
          <div className="section">
            <div className=" sub-section">
              <div className="style">
                <span className="head">Freelancer</span>
                <span className="b-text">2009-2012</span>
                <br />
                <span className="color-text">Senior Developer</span>
                <hr></hr>

                <p>
                  Lorem ipsum dolor sit amet, in quodsi vulputate pro. Ius illum
                  vocent mediocritatem an, cule dicta iriure at. Ubique
                  maluisset vel te, his dico vituperata ut. Pro ei phaedrum
                  maluisset. Ex audire suavitate has, ei quodsi tacimates
                  sapientem sed, pri zril ubique ut. Lorem ipsum dolor sit amet,
                  in quodsi.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="ex-contain">
          <div className="section">
            <div className="sub-section">
              <div className="style">
                <span className="head">Front-end Course.</span>
                <span className="b-text">PRESENT</span>
                <br />
                <span className="color-text">london</span>
                <hr></hr>
                <p>
                  Lorem ipsum dolor sit amet, in quodsi vulputate pro. Ius illum
                  vocent mediocritatem an, cule dicta iriure at. Ubique
                  maluisset vel te, his dico vituperata ut. Pro ei phaedrum
                  maluisset. Ex audire suavitate has, ei quodsi tacimates
                  sapientem sed, pri zril ubique ut. Lorem ipsum dolor sit amet,
                  in quodsi.
                </p>
              </div>
            </div>
          </div>
          <div className="section">
            <div className=" sub-section">
              <div className="style">
              <FontAwesomeIcon className="faicon" icon="graduation-cap" />
                <span className="head">Art University</span>
                <span className="b-text">2014-2016</span>
                <br />
                <span className="color-text">Newyork</span>
                <hr></hr>

                <p>
                  Lorem ipsum dolor sit amet, in quodsi vulputate pro. Ius illum
                  vocent mediocritatem an, cule dicta iriure at. Ubique
                  maluisset vel te, his dico vituperata ut. Pro ei phaedrum
                  maluisset. Ex audire suavitate has, ei quodsi tacimates
                  sapientem sed, pri zril ubique ut. Lorem ipsum dolor sit amet,
                  in quodsi.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default experience;
