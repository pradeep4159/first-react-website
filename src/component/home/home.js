import React, { Component } from "react";
import "./home.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class home extends Component {
  render() {
    return (
      <div className="home" id="home-container">
        <div className="home-box">
          <div className="home-back">
            <img src="../assert/image/user.jpg" />
          </div>
          <div className="home-image">
            <div className="div-part part">
              <img src="../assert/image/avatar.jpg" />
            </div>
            <div className="div-part part">
              <p className="home-text">Sean Stevenson</p>
              <p className="home-p">Front-end Developer</p>
            </div>
            <div className="div-part h-wid">
              <p className="home-text">96%</p>
              <p className="home-p">Jobs Completed</p>
            </div>
            <div className="div-part h-wid">
              <p className="home-text">10</p>
              <p className="home-p">Years Experience</p>
            </div>
            <div className="div-part h-wid">
              <p className="home-text"> $25</p>
              <p className="home-p">USD/hr</p>
            </div>
          </div>
        </div>
        <div className="home-card">
            <div className="color-card">
            <span><FontAwesomeIcon className="fa" icon="user" /></span>
            <span className="fa-text">400+</span>
             <p>Happy clients</p>
            </div>
            <div className="color-card card-2">
            <span><FontAwesomeIcon className="fa" icon="check-double" /></span>
            <span className="fa-text">480+</span>
             <p>Complated Projects</p>
            </div>
            <div className="color-card card-3">
            <span><FontAwesomeIcon className="fa" icon="check-double" /></span>
            <span className="fa-text">10000+</span>
             <p>Lines of code</p>
           </div>
           <div className="color-card ho-co">
           <span><FontAwesomeIcon className="fa" icon="check-double" /></span>
            <span className="fa-text">400+</span>
             <p>Happy clients</p>
           </div>

        </div>
      </div>
    );
  }
}

export default home;
