import React, { Component } from "react";
import "./about.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class about extends Component {
  render() {
    return (
      <div className="main-div" id="about-cont">
        <div>
          <div className="about-me">About Me</div>
        </div>
        <div className="container">
        <div className="text">
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
            quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
            aspernatur aut odit aut fugit. Sed ut perspiciatis unde omnis iste
            natus error sit voluptatem accusantium doloremque.
          </p>
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
            quae ab illo inventore veritatis et quasi architecto beatae vitae
            dicta sunt explicabo.
          </p>
          <div className="about-btn">
            <button className="btn">DOWNLOAD CV</button>
          </div>
          <div className="spacer"></div>
        </div>

        </div>
        <div>
          <div className="container">
            <div className="about-layout">
              <div className="bottom ">
                <span className="text-color">
                  <FontAwesomeIcon className="faicons" icon="user" />
                </span>
                <span className="text-color">age:</span>
                <span className="abt-text">22years</span>
              </div>
              <div className="bottom">
                <span className="text-color">
                  <FontAwesomeIcon className="faicons" icon="briefcase" />
                </span>
                <span className="text-color">job:</span>
                <span className="abt-text">freelancer</span>
              </div>
              <div className="bottom">
                <span>
                  <FontAwesomeIcon className="faicons" icon="mapsigns" />
                </span>
                <span className="text-color">citizenship:</span>
                <span className="abt-text">united states</span>
              </div>
              <div className="bottom">
                <span>
                  <FontAwesomeIcon className="faicons" icon="faAddressCard" />
                </span>
                <span className="text-color">Adress:</span>
                <span className="abt-text">
                  358 W Jefferson St, Bensenville
                </span>
              </div>
            </div>

            <div className="about-layout ">
              <div className="bottom">
                <span className="text-color">cups of coffe:</span>
                <span className="abt-text">2000.00</span>
              </div>
              <div className="bottom">
                <span className="text-color">countries visited:</span>
                <span className="abt-text">28</span>
              </div>
              <div className="bottom">
                <span className="text-color">Awards won:</span>
                <span className="abt-text">16</span>
              </div>
              <div className="bottom">
                <span className="text-color">Albums listened:</span>
                <span className="abt-text">80</span>
              </div>
            </div>
          </div>
          <div>
            <div className="container">
            <div className="about-layout ">
              <div className="second">
                <p>Professional Skills</p>
                <p className="se-color">Sed ut perspiciatis</p>
              </div>
              <div className="second">
                <label for="file">UI/UX Design</label>
                <progress className="abt-text" id="file" value="72" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Web Delevopment</label>
                <progress className="abt-text" id="file" value="92" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Mobile Application</label>
                <progress className="abt-text" id="file" value="85" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Writing</label>
                <progress className="abt-text" id="file" value="60" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Photography</label>
                <progress className="abt-text" id="file" value="70" max="100">
                </progress>
              </div>
            </div>
            <div className="about-layout ">
              <div className="second">
                <p>Professional Skills</p>
                <p className="se-color">Sed ut perspiciatis</p>
              </div>
              <div className="second">
                <label for="file">UI/UX Design</label>
                <progress className="abt-text" id="file" value="92" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Web Delevopment</label>
                <progress className="abt-text" id="file" value="80" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Mobile Application</label>
                <progress className="abt-text" id="file" value="88" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Writing</label>
                <progress className="abt-text" id="file" value="70" max="100">
                </progress>
              </div>
              <div className="second">
                <label for="file">Photography</label>
                <progress className="abt-text" id="file" value="75" max="100">
                </progress>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default about;
