import React, { Component } from 'react'
import './contact.css';

export class contact extends Component {
    render() {
        return (
            <div className="contact-div" id="contact-cont">
                <div className="heading">
                    <p>Contact Me</p>
                </div>
                <div className="contact">
                    <div className="contact-detail">
                        <div className="contact-ad">
                            <span>Address</span>
                            <span className="con-text">358 W Jefferson St, Bensenville</span>
                        </div>
                        <div className="contact-ad">
                            <span>Phone</span>
                            <span className="con-text-color" >+1 256 254 84 56</span>
                        </div>
                        <div className="contact-ad">
                            <span>E-mail</span>
                            <span className="con-text-color">smorgan@domain.com</span>
                        </div>
                        <div className="contact-ad">
                            <span>E-mail</span>
                            <span className="con-text-color">smorgan@domain.com</span>
                        </div>
                        <div className="contact-ad">
                            <span>E-mail</span>
                            <span className="con-text-color">smorgan@domain.com</span>
                        </div>
                    </div>
                    <div className="contact-detail">
                        <div className="contact-head">
                           <p>Write a Message:</p>
                        </div>
                        <div className="con-input">
                            <label></label>
                            <input type="text" placeholder="Your Name"/>
                        </div> 
                        <div className="con-input">
                            <label></label>
                            <input type="text" placeholder="Your Email"/>
                        </div>
                        <div className="con-input">
                            <label></label>
                            <input type="text" placeholder="Subject"/>
                        </div>
                        <div className="con-input">
                            
                          <textarea placeholder="message"/>
                        </div>
                        <div className="con-button">
                            <button className="contact-but">SEND MESSAGE</button>
                        </div>
                    </div>
                </div>
                <div className="foot-text">
                    <p>© 2016 mateCard. All rights reserved.</p>
                </div>
                
            </div>
        )
    }
}

export default contact;
