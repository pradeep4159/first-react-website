import React, { Component } from "react";
import "./service.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class service extends Component {
  render() {
    return (
      <div className="service-main" id="service-cont">
        <div className="heading">
          <p>Services</p>
        </div>
        <div className="service">
          <div className="service-card">
            <div className="service-flex">
              <div className="service-font">
                <FontAwesomeIcon className="faic" icon="grip-horizontal" />
              </div>
              <div className="service-icon">
                 <div className="serv-m">
                     <p>UI/UX Design</p>
                 </div>
                 <div className="serv-color">
                     <p>I am a very simple card. I am good at <br/> containing small bits of information.</p>
                 </div>
              </div>
            </div>
          </div>
          <div className="service-card">
          <div className="service-flex">
              <div className="service-f">
                <FontAwesomeIcon className="faic" icon="id-card" />
              </div>
              <div className="service-icon">
                 <div className="serv-m">
                     <p>Web Application</p>
                 </div>
                 <div className="serv-color">
                     <p>I am a very simple card. I am good at <br/> containing small bits of information.</p>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <div className="service">
          <div className="service-card">
            <div className="service-flex">
              <div className="service-t">
                <FontAwesomeIcon className="faic" icon="grip-horizontal" />
              </div>
              <div className="service-icon">
                 <div className="serv-m">
                     <p>Android Application</p>
                 </div>
                 <div className="serv-color">
                     <p>I am a very simple card. I am good at <br/> containing small bits of information.</p>
                 </div>
              </div>
            </div>
          </div>
          <div className="service-card">
          <div className="service-flex">
              <div className="service-o">
                <FontAwesomeIcon className="faic" icon="mobile" />
              </div>
              <div className="service-icon">
                 <div className="serv-m">
                     <p>Mobile Design</p>
                 </div>
                 <div className="serv-color">
                     <p>I am a very simple card. I am good at <br/> containing small bits of information.</p>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <div className="service">
          <div className="service-card">
            <div className="service-flex">
              <div className="service-n">
                <FontAwesomeIcon className="faic" icon="pen" />
              </div>
              <div className="service-icon">
                 <div className="serv-m">
                     <p>Writing</p>
                 </div>
                 <div className="serv-color">
                     <p>I am a very simple card. I am good at <br/> containing small bits of information.</p>
                 </div>
              </div>
            </div>
          </div>
          <div className="service-card">
          <div className="service-flex">
              <div className="service-fo">
                <FontAwesomeIcon className="faic" icon="camera" />
              </div>
              <div className="service-icon">
                 <div className="serv-m">
                     <p>Photography</p>
                 </div>
                 <div className="serv-color">
                     <p>I am a very simple card. I am good at <br/> containing small bits of information.</p>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default service;
