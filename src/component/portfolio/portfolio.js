import React, { Component } from "react";
import "./portfolio.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class portfolio extends Component {
  render() {
    return (
      <div className="port-main" id="portfolio-cont">
          <div className="heading">
               <p>Portfolio</p>
          </div>
          <div className="port-header">
              <ul className="port-detail">
                  <li>ALL</li>
                  <li>MOCKUPS</li>
                  <li>GRAPHICS</li>
                  <li>ICONS</li>
                  <li>UIKITS</li>
              </ul>

          </div>
        <div className="card-content">
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/work1.jpg" />
            </div>
            <div className="port-text">
              <span className="">Realistic Business</span>
              <span className="port-color">
                  <FontAwesomeIcon className="faicons" icon="ellipsis-v" />
                  </span>
            </div>
            <div className="mock-color">
                <p>Mockups</p>
            </div>
          </div>
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/work3.jpg" />
            </div>
            <div className="port-text">
              <span className="">The Mountainbiker </span>
              <span className="port-color">
                  <FontAwesomeIcon className="faicons" icon="ellipsis-v" />
                  </span>
            </div>
            <div className="mock-color" >
                <p>Graphics</p>
            </div>
          </div>
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/work2.jpg" />
            </div>
            <div className="port-text">
              <span className="">Notebook MockUp</span>
              <span className="port-color">
                  <FontAwesomeIcon className="faicons" icon="ellipsis-v" />
                </span>
            </div>
            <div className="mock-color">
                <p>Mockups</p>
            </div>
          </div>
        </div>
        <div className="card-content">
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/work4.jpg" />
            </div>
            <div className="port-text">
              <span className="">Capitalist Icons</span>
              <span className="port-color">
                  <FontAwesomeIcon className="faicons" icon="ellipsis-v" />
                </span>
            </div>
            <div className="mock-color">
                <p>Icons</p>
            </div>
          </div>
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/work5.jpg" />
            </div>
            <div className="port-text">
              <span className="">Clean and Modern </span>
              <span className="port-color">
                  <FontAwesomeIcon className="faicons" icon="ellipsis-v" />
                </span>
            </div>
            <div className="mock-color">
                <p>UI Kits</p>
            </div>
          </div>
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/work6.jpg" />
            </div>
            <div className="port-text">
              <span className="">Chameleon UI Kit</span>
              <span className="port-color">
                  <FontAwesomeIcon className="faicons" icon="ellipsis-v" />
                </span>
            </div>
            <div className="mock-color">
                <p>UI kits</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default portfolio;
