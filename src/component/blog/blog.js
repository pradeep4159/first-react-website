import React, { Component } from "react";
import "./blog.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class blog extends Component {
  render() {
    return (
      <div className="blog-main" id="blog-cont">
        <div className="heading">
          <p>Blog</p>
        </div>
        <div className="card-content">
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/blog1.jpg" />
            </div>
            <div>   <FontAwesomeIcon className="icons" icon="ellipsis-v" /></div>
            <div className="card-text">
              <span className="card-title">Music Player Design</span>
              <p>
                Lorem ipsum dolor sit amet,<br/> consectetur adipiscing elit. Morbi <br/>
                venenatis et tortor ac tincidunt.<br/> In euismod iaculis lobortis.
                Vestibulum <br/> posuere molestie ipsum vel...
              </p>
            </div>
            <div className="card-action">
                <span className="cd-back">branding</span>
                <span className="cd-back">ui-ux</span>
                <span className="cd-text">27 july</span>
            </div>
          </div>
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/blog2.jpg" />
            </div>
            <div>   <FontAwesomeIcon className="icons" icon="ellipsis-v" /></div>
            <div className="card-text">
          
              <span className="card-title">Validate HTML Code</span>
              <p>
              Lorem ipsum dolor sit amet,<br/> consectetur adipiscing elit. Morbi <br/>
                venenatis et tortor ac tincidunt.<br/> In euismod iaculis lobortis.
                Vestibulum <br/> posuere molestie ipsum vel...
              </p>
            </div>
            <div className="card-action">
                <span className="cd-back">html</span>
                <span className="cd-back">code</span>
                <span className="cd-text">18 july</span>
            </div>
          </div>
          <div className="card">
            <div className="card-image">
              <img src="../assert/image/blog3.jpg" />
            </div>
            <div className="card-text">
              <span className="card-title">Music Player Design</span>
              <p>
              Lorem ipsum dolor sit amet,<br/> consectetur adipiscing elit. Morbi <br/>
                venenatis et tortor ac tincidunt.<br/> In euismod iaculis lobortis.
                Vestibulum <br/> posuere molestie ipsum vel...
              </p>
            </div>
            <div className="card-action">
                <span className="cd-back">branding</span>
                <span className="cd-back">ui-ux</span>
                <span className="cd-text">27 july</span>
            </div>
          </div>
        </div>
        <div className="view-button">
              <button className="view">VIEW BLOG</button>
          </div>
      </div>
    );
  }
}

export default blog;
