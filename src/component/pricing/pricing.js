import React, { Component } from "react";
import "./pricing.css";

export class pricing extends Component {
  render() {
    return (
      <div className="main-div" id="price-cont">
        <div className="heading">
          <p className="pr-text"> Pricing</p>
        </div>
        <div className="pricing">
          <div className="price-layout">
            <div className="p-layout">
              <div>
                <p className="font"> Basic</p>
              </div>
              <div>  
                  <sup>$</sup>  <span> <h1>15</h1></span><sub>/hr</sub>
              </div>
              <div>
                <p className="text-decor">UI/UX Design</p>
              </div>
              <div>
                <p className="pr-text">Web Development</p>
              </div>
              <div>
                <p className="text-decor">IOS Application</p>
                <span className="color-text">new</span>
              </div>
              <div>
                <p className="text-decor">Android Application</p>
              </div>
              <div>
                <p className="pr-text">Writing</p>
              </div>
              <div>
                <span className="text-decor">photography</span>
                <span className="color-text">new</span>
              </div>
              <div className="buy-now">
                <button className="butt-buy">BUY NOW</button>
              </div>
            </div>
          </div>
          <div className="price-layout">
            <div className="p-layout">
              <div>
                <p className="font"> Start-Up</p>
              </div>
              <div>
                <span>
                  <sup>$</sup>  <span> <h1>15</h1></span><sub>/hr</sub>
                </span>
              </div>
              <div>
                <p className="pr-text">UI/UX Design</p>
              </div>
              <div>
                <p className="pr-text">Web Development</p>
              </div>
              <div>
                <p className="text-decor">IOS Application</p>
                <span className="color-text">new</span>
              </div>
              <div>
                <p className="text-decor">Android Application</p>
              </div>
              <div>
                <p className="text-decor">Writing</p>
              </div>
              <div>
                <span className="text-decor">photography</span>
                <span className="color-text">new</span>
              </div>
              <div className="buy-now">
                <button className="butt-buy">BUY NOW</button>
              </div>
            </div>
          </div>
          <div className="price-layout">
            <div className="p-layout">
              <div>
                <p className="font"> Business</p>
              </div>
              <div>
                <span>
                  <sup>$</sup>  <span> <h1>15</h1></span><sub>/hr</sub>
                </span>
              </div>
              <div>
                <p className="pr-text">UI/UX Design</p>
              </div>
              <div>
                <p className="pr-text">Web Development</p>
              </div>
              <div>
                <p className="pr-text">IOS Application</p>
                <span className="color-text">new</span>
              </div>
              <div>
                <p className="pr-text">Android Application</p>
              </div>
              <div>
                <p className="text-decor">Writing</p>
              </div>
              <div>
                <span className="text-decor">photography</span>
                <span className="color-text">new</span>
              </div>
              <div className="buy-now">
                <button className="butt-buy">BUY NOW</button>
              </div>
            </div>
          </div>
          <div className="price-layout">
            <div className="p-layout">
              <div>
                <p className="font"> Enterprise</p>
              </div>
              <div>
                <span>
                  <sup>$</sup>  <span> <h1>15</h1></span><sub>/hr</sub>
                </span>
              </div>
              <div>
                <p className="pr-text">UI/UX Design</p>
              </div>
              <div>
                <p className="pr-text">Web Development</p>
              </div>
              <div>
                <p className="pr-text">IOS Application</p>
                <span className="color-text">new</span>
              </div>
              <div>
                <p className="pr-text">Android Application</p>
              </div>
              <div>
                <p className="pr-text">Writing</p>
              </div>
              <div>
                <span className="text-decor">photography</span>
                <span className="color-text">new</span>
              </div>
              <div className="buy-now">
                <button className="butt-buy">BUY NOW</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default pricing;
