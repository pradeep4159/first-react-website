
import './App.css';
 import Fontawesomeicon from './component/Fontawesomeicon/font';
import Navbar from'./component/navbar/navbar';
import About from './component/about/about';
import Experience from './component/experience/experience';
import Pricing from './component/pricing/pricing';
import Blog from './component/blog/blog';
import Contact from './component/contact/contact';
import Portfolio from './component/portfolio/portfolio';
import Service from './component/service/service';
import Home from './component/home/home';




function App() {
  return (
  
      <div className="App">
      <div className="position">
        <Navbar/>
      </div>
      <div className="fixed overflow-auto">
         <Home/> 
         <About/> 
        <Experience/>
        <Service/>
        <Portfolio/>
        <Pricing/>
        <Blog/>
        <Contact/> 
      </div>
   
    </div>
 
  );
}

export default App;
